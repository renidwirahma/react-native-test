import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import Navigator from './module/RootNavigation/RootNavigator';

const App = () => {
    return (
        <NavigationContainer>
            <Navigator></Navigator>
        </NavigationContainer>

    );
};

export default App;