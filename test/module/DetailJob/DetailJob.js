import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, ScrollView, TouchableOpacity, Linking } from 'react-native'
import Images from '../assets/Images'

class DetailJob extends React.Component {
  render() {
    this.item = this.props.route.params.item;
    console.log("DETAIL", this.item)
    return (
      <View style={{ backgroundColor: 'white', flex: 1 }}>
        <View style={{ width: '100%', height: 57, backgroundColor: '#6A88CE', justifyContent: 'center' }}>
          <View style={{ flexDirection: 'row', }}>
            <TouchableOpacity onPress={()=> this.props.navigation.goBack()}>
              <Image source={Images.back} style={{ width: 22, height: 22, marginLeft: 20 }}></Image>
            </TouchableOpacity>

            <Text style={{
              textAlign: 'center', color: 'white', fontSize: 18, fontWeight: 'bold',
              marginLeft: 110
            }}>Detail Job</Text>


          </View>

        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Text style={{
            fontSize: 16, fontWeight: 'bold', color: '#656565',
            marginTop: 30, paddingHorizontal: 30
          }}>Company</Text>
          <View style={styles.card1}>
            <View style={{ flexDirection: 'row' }}>

              <Image source={{ uri: this.item.company_logo }}
                style={{ width: 126, height: 76, resizeMode: 'contain', marginTop: 20 }}></Image>
              <View style={{}}>
                <Text style={{ maxWidth: '70%', marginTop: 8, fontWeight: 'bold', fontSize: 16 }}>{this.item.company}</Text>
                <Text style={{ maxWidth: '70%', marginTop: 8 }}>{this.item.title}</Text>
                <TouchableOpacity onPress={() => Linking.openURL(this.item.company_url)}>
                  <Text style={{ marginTop: 15, color: '#6A88CE' }}>Go To Website</Text>
                </TouchableOpacity>

              </View>

            </View>

          </View>

          <Text style={{
            fontSize: 16, fontWeight: 'bold', color: '#656565',
            marginTop: 30, paddingHorizontal: 30
          }}>Job Spesification</Text>
          <View style={styles.card2}>
            <Text style={{ fontSize: 14, marginBottom: 5, fontWeight: 'bold' }}>Title</Text>
            <Text style={{ fontSize: 14, marginBottom: 10, color: '#656565' }}>{this.item.title}</Text>

            <Text style={{ fontSize: 14, marginBottom: 5, fontWeight: 'bold', marginTop: 20 }}>Fulltime</Text>
            <Text style={{ fontSize: 14, marginBottom: 10, color: '#656565' }}>{this.item.type}</Text>

            <Text style={{ fontSize: 14, marginBottom: 5, fontWeight: 'bold', marginTop: 20 }}>Description</Text>
            <Text style={{ fontSize: 14, marginBottom: 10, color: '#656565' }}>{this.item.description}</Text>
          </View>
          <View style={{ marginBottom: 80 }}></View>
        </ScrollView>



      </View>
    )
  }
}

export default DetailJob

const styles = StyleSheet.create({
  card1: {
    backgroundColor: '#F1EFEF',
    width: '90%',
    height: 160,
    marginHorizontal: 20,
    borderWidth: 1,
    borderColor: '#D1D1D1',
    borderRadius: 10,
    marginVertical: 10,
    padding: 10,
    marginTop: 20
  },
  card2: {
    backgroundColor: '#F1EFEF',
    width: '90%',
    height: 1600,
    marginHorizontal: 20,
    borderWidth: 1,
    borderColor: '#D1D1D1',
    borderRadius: 10,
    marginVertical: 10,
    padding: 10,
    marginTop: 20
  },
})
