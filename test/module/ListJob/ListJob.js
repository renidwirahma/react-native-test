import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import ListJobScreen from './screens/ListJobScreen'

class ListJob extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      jobData:[],
      isLoading:false
    }
  }

  componentDidMount () {
    this.getListJob();
  }

  getListJob = () => {
    this.setState({ isLoading: true })
    let url = "https://jobs.github.com/positions.json?page=1"
    console.log("URL", url)
    fetch(url)
      .then(response => response.json())
      .then((responseJson) => {
        // console.log("RESULT Again : ", responseJson)
        this.setState({ isLoading: false })
        //  this.jobData = responseJson;
         this.setState({jobData: responseJson})
      })
      .catch(error => {
        this.setState({ isLoading: false })
        console.log('ERROR', error)
      })
  }

  getSearch = (txtSearch, fullTime, location) => {
    // txtSearch = '';
    // fullTime = false;
    // location = '';

      console.log('cari text ' + txtSearch + fullTime + location);
      this.setState({ isLoading: true});
      fetch('https://jobs.github.com/positions.json?description=' + txtSearch + '&full_time=' + fullTime + '&location='+location)
        .then(response => response.json())
        .then((responseJson) => {
          console.log("Result Search : ", responseJson);
          this.setState({ isLoading: false});
          this.setState({jobData: responseJson})
        })
        .catch(error => {
          this.setState({ isLoading: false});
          console.log('Failed Get Customer:', error);
        })
   
  }

   format(data){
    var result = []
    if(!Array.isArray(data)){//if is not an array
       result.push(data) // push it as the first item of an array
    }else{
     result=data
    }
    
    return result
    }


  render() {
    
    return (
      <ListJobScreen
        isLoading={this.state.isLoading}
        navigation={this.props.navigation}
        jobData={this.format(this.state.jobData)}
        onSearch={(txtSearch, fullTime, location) => {
          console.log('text yg dikirim', txtSearch, fullTime, location);
          this.getSearch(txtSearch, fullTime, location);
        }}
      ></ListJobScreen>
    )
  }
}

export default ListJob

const styles = StyleSheet.create({})
