import React, { Component } from 'react'
import { StyleSheet, Text, View, TextInput, Image, Switch, FlatList } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import Images from '../../assets/Images'
import ItemJob from '../../components/ItemJob'

import Loading from '../../components/Loading'

class ListJobScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      contentSearch: false,
      switchValue: false,
      txtSearch: '',
      isLoading: false,
      location:''

    }
  }

  ContentSearch = () => {
    this.setState({ contentSearch: true })
  }
  close = () => {
    this.setState({ contentSearch: false })
  }

  render() {
    console.log('DATA', this.props.jobData)
    return (
      <View style={{ backgroundColor: 'white', flex: 1 }}>
        <View style={{ width: '100%', height: 57, backgroundColor: '#6A88CE', justifyContent: 'center' }}>
          <Text style={{ textAlign: 'center', color: 'white', fontSize: 18, fontWeight: 'bold' }}>List Job</Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <View style={{
            margin: 20, backgroundColor: '#EBEBEB', width: '80%', height: 42,
            borderRadius: 10, justifyContent: 'center', paddingLeft: 10
          }}>
            <View style={{ flexDirection: 'row' }}>
              <Image source={Images.search} style={{ width: 20, height: 20, marginTop: 5 }}></Image>
              <TextInput
                placeholder="   Search Here.."
                style={{ padding: 0, }}
                onChangeText={val => { this.setState({ txtSearch: val }); }}
                // onSubmitEditing={val => { this.props.onSearch(txtSearch, switchValue, location) }}
                returnKeyType='done'
              ></TextInput>
            </View>
          </View>

          <TouchableOpacity onPress={() => this.ContentSearch()}>
            <Image source={Images.arrowBottom} style={{ width: 24, height: 24, marginTop: 30 }}></Image>
          </TouchableOpacity>

        </View>

        {this.state.contentSearch && (
          <View style={styles.card1}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ width: '95%' }}></View>
              <TouchableOpacity onPress={() => this.close()}>
                <Text style={{
                  color: 'red', fontSize: 18,
                  justifyContent: 'flex-end', flexDirection: 'row', right: 0
                }}>X</Text>
              </TouchableOpacity>

            </View>

            <View style={{ flexDirection: 'row' }}>
              <Text>Fulltime</Text>
              <Switch
                style={{ marginLeft: 40 }}
                value={this.state.switchValue}
                trackColor={{ false: '#6F6E6E', true: '#6A88CE' }}
                thumbColor="#FFFFFF"
                onValueChange={(switchValue) => this.setState({ switchValue })}
              ></Switch>
            </View>
            <View style={{ flexDirection: 'row', marginTop: 10 }}>
              <Text>Location</Text>
              <TextInput
                style={{
                  backgroundColor: '#C4C4C4', borderRadius: 5, width: 195,
                  height: 31, marginLeft: 40, padding:0
                }}
                onChangeText={val => { this.setState({ location: val }); }}
                
              ></TextInput>

            </View>
            <TouchableOpacity style={{
              backgroundColor: '#6A88CE', width: 104, height: 24,
              marginTop: 16, borderRadius: 10, alignSelf: 'center', justifyContent: 'center'
            }} onPress={()=> {this.props.onSearch(this.state.txtSearch, this.state.switchValue, this.state.location)}}>
              <Text style={{ color: 'white', fontSize: 11, fontWeight: 'bold', textAlign: 'center' }}>Apply</Text>
            </TouchableOpacity>
          </View>
        )}

        <View >
          <FlatList
            showsVerticalScrollIndicator={false}
            data={this.props.jobData}
            extraData={this.state}
            keyExtractor={item => item.id}
            renderItem={({ item }) => {
              // console.log("ITEM LIST", item)
              // console.log("Data Job : ", this.props.jobData);
              return (
                <View style={styles.card1}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate("DetailJob", { item })}>
                    <View >
                      <ItemJob
                        imgUrl={item.company_logo}
                        title={item.title}
                        company={item.company}
                        location={item.location}
                      />
                    </View>
                  </TouchableOpacity>
                </View>
              )

            }}

          />
          <View style={{marginBottom:80}}></View>

        </View>

        <Loading isVisible={this.props.isLoading} title="Loading"></Loading>

      </View>
    )
  }
}

export default ListJobScreen

const styles = StyleSheet.create({

  card1: {
    backgroundColor: '#F1EFEF',
    width: '90%',
    height: 160,
    marginHorizontal: 20,
    borderWidth: 1,
    borderColor: '#D1D1D1',
    borderRadius: 10,
    marginVertical: 10,
    padding: 10
  },
})
