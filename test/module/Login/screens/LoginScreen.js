import React, { Component } from 'react'
import {
  StyleSheet, Text, View, Image, TextInput, TouchableOpacity,
  Modal, ScrollView
} from 'react-native'

import Images from '../../assets/Images'
import Loading from '../../components/Loading'

class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      alertVisibility: false,
      alertContentTitle: 'Request Failed',
      alertContentInfo: 'Email Format is not valid',
      secureTextEntry: true,
    }
  }

  handleUsername = (val) => {
    this.setState({ username: val })
  }

  handlePassword = (val) => {
    this.setState({ password: val })
  }


  submit = (username, password) => {
    if (username === '') {
      this.setState({
        alertContentTitle: 'Request Failed',
        alertContentInfo: 'Username is empty',
        alertVisibility: true
      })
    } else if (password === '') {
      this.setState({
        alertContentTitle: 'Request Failed',
        alertContentInfo: 'Password is empty',
        alertVisibility: true
      })
    } else {
      this.setState({ loadingVisibility: true });
      this.props.navigation.navigate("ListJob")
    }
  }

  render() {
    const { secureTextEntry } = this.state;
    const {
      username, password, alertVisibility, alertContentTitle, alertContentInfo,
    } = this.state;
    return (
      <View style={styles.bg}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{ justifyContent: 'center', alignSelf: 'center', marginTop: 50 }}>
            <Text style={{
              marginTop: 10, color: '#1040B0', fontSize: 20,

            }}>Welcome to </Text>
            <Text style={{
              color: '#1040B0', fontSize: 20,marginTop:6,
              fontWeight: 'bold', marginBottom: 40
            }}>Mobile Jobs</Text>
            <Image source={Images.login} style={styles.imgLogin}></Image>

            <Text style={{ marginTop: 10, marginBottom: 10, color: '#4F4F4F', fontWeight: 'bold' }}>Username</Text>
            <View style={{ width: 258, height: 36 }}>
              <TextInput
                style={{ backgroundColor: '#DEDEDE', borderRadius: 10 }}
                keyboardType='email-address'
                autoCapitalize='none'
                onChangeText={(val) => this.handleUsername(val)}
              ></TextInput>
            </View>

            <Text style={{ marginTop: 20, marginBottom: 10, color: '#4F4F4F', fontWeight: 'bold' }}> Password</Text>
            <View style={{ width: 258, height: 36 }}>
              <TextInput
                style={{ backgroundColor: '#DEDEDE', borderRadius: 10 }}
                autoCapitalize='none'
                secureTextEntry={secureTextEntry}
                onChangeText={(val) => this.handlePassword(val)}
              ></TextInput>
            </View>

            <TouchableOpacity style={{
              backgroundColor: '#6A88CE', width: 200, height: 43,
              borderRadius: 10, marginTop: 52, justifyContent: 'center', alignSelf: 'center'
            }}
              onPress={() => this.submit(username, password)}>
              <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 14, textAlign: 'center' }}>Login</Text>
            </TouchableOpacity>

          </View>
        </ScrollView>


        <Loading isVisible={this.props.isLoading} title="Loading"></Loading>

        <Modal
          visible={alertVisibility}
          transparent={true}
          animationType={'fade'}
          onRequestClose={() => { this.setState({ alertVisibility: false }); }}
          style={styles.containerAlert}
        >
          <View style={styles.contModal}>
            <View style={styles.containerContentInAlert}>
              {/* Title */}
              <Text style={styles.titleInAlert}>{alertContentTitle}</Text>

              {/* Information */}
              <Text style={styles.informationInAlert}>{alertContentInfo}</Text>

              {/* Button OK */}
              <TouchableOpacity onPress={() => this.setState({ alertVisibility: false })}>
                <Text>OK</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

      </View>
    )
  }
}

export default LoginScreen

const styles = StyleSheet.create({
  bg: {
    flex: 1,
    backgroundColor: 'white',
    padding: 20
  },
  imgLogin: {
    width: 268,
    height: 192,
    resizeMode: 'contain'
  },

  containerAlert: {
    justifyContent: 'flex-end',
    margin: 15
  },
  contModal: {
    flex: 1,
    justifyContent: 'center',
    padding: 10,
    backgroundColor: '#00000080'
  },
  containerContentInAlert: {
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 20
  },
  titleInAlert: {
    color: 'black',
    fontSize: 18,
    marginBottom: 10,
    textDecorationLine: 'underline'
  },
  informationInAlert: {
    color: 'black',
    fontSize: 14,
    marginBottom: 15
  },
  ok: {
    alignSelf: 'center',
    color: '#C4C4C4',
    fontSize: 14,
    textAlign: 'center',
    textDecorationLine: 'underline'
  },
})
