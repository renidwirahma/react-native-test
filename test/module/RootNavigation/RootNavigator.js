import React from 'react'
import { Button, View, Text } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

import Login from '../Login/screens/LoginScreen'
import ListJob from '../ListJob/ListJob'
import DetailJob from '../DetailJob/DetailJob'

const Stack = createStackNavigator()

const RootNavigator = () => {
  return (
    <Stack.Navigator initialRouteName='Login'>
      <Stack.Screen
        name='Login'
        component={Login}
        options={{ headerShown: false }}
      ></Stack.Screen>

      <Stack.Screen
        name='ListJob'
        component={ListJob}
        options={{ headerShown: false }}
      ></Stack.Screen>

      <Stack.Screen
        name='DetailJob'
        component={DetailJob}
        options={{ headerShown: false }}
      ></Stack.Screen>
    </Stack.Navigator>
  )
}

export default RootNavigator

