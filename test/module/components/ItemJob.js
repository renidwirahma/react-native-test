import React, {Component} from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import Images from '../assets/Images'

class ItemJob extends React.Component{
  constructor(props) {
    super(props)
  }
  
  render() {
    return (
      <View style={{ flexDirection: 'row' }}>
        <View style={{ flexDirection: 'row', flex: 1 }}>
          <Image source={{ uri: this.props.imgUrl }} style={{
            width: 126, height: 176, resizeMode: 'contain',
            marginTop: -30
          }}></Image>
          <View style={{ marginLeft: 40, marginTop: 10 }}>
            <Text style={{ fontWeight: 'bold', fontSize: 16, maxWidth:'70%' }}>{this.props.title}</Text>
            <Text style={{ fontSize: 13,  maxWidth:'70%' }}>{this.props.company}</Text>
            <Text style={{ maxWidth:'70%'}}>{this.props.location}</Text>
          </View>
        </View>

        <View style={{ alignSelf: 'center', marginTop: -30 }}>
          <Image source={Images.arrowRight} style={{
            width: 17, height: 18, resizeMode: 'contain',
          }}></Image>
        </View>

      </View>
   
    )
  }
}

ItemJob.defaultProps = {
  imgUrl:'',
  title:'',
  company:'',
  location:''
}

export default ItemJob

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#F1EFEF',
    width: '90%',
    height: 130,
    marginHorizontal: 20,
    borderWidth: 1,
    borderColor: '#D1D1D1',
    borderRadius: 10,
    marginVertical: 10,
    padding: 10
  },

})
