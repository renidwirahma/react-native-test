import React from 'react';
import { Image, View, Text, TouchableOpacity, Modal, ActivityIndicator } from 'react-native';

class Loading extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (

      <Modal
        visible={this.props.isVisible}
        animationType={'fade'}
        backdropColor={'#00000080'}
        transparent={true}
      >

        <View style={styles.contModal}>
          <View style={styles.containerContentInAlert}>
            <Text style={{ textAlign: 'center', fontSize: 15, marginBottom: 7 }}>{this.props.title}</Text>
            <ActivityIndicator size="large" color={'#6A88CE'} />
          </View>
        </View>

      </Modal>
    )
  }
}
const styles = {
  contModal: {
    flex: 1,
    justifyContent: 'center',
    padding: 70,
    backgroundColor: '#00000080'
  },
  containerContentInAlert: {
    backgroundColor: 'white',
    borderRadius: 5,
    padding: 20

  }
}
Loading.defaultProps = {
  isVisible: false,
  title: "Loading"
}

export default Loading;